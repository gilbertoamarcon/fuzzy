#include "fuzzyVar.hpp"

FuzzyVar::FuzzyVar(){
	this->memFuncType	= 0;
	this->numMemFunc	= 0;
	this->memFuncVal	= NULL;
	this->memFuncLim	= NULL;
}

FuzzyVar::~FuzzyVar(){
	delete[] memFuncVal;
	delete[] memFuncLim;
}

void FuzzyVar::init(int memFuncType,int numMemFunc,double *memFuncLim){
	this->memFuncType	= memFuncType;
	this->numMemFunc	= numMemFunc;
	this->memFuncVal	= new double[numMemFunc];
	this->memFuncLim	= new double[numMemFunc];
	for(int m = 0; m < numMemFunc; m++){
		this->memFuncVal[m]	= 0;
		this->memFuncLim[m]	= memFuncLim[m];
	}
}

void FuzzyVar::eval(double crispInput){
	double beg = 0;	// Membership function lower bound
	double mid = 0;	// Membership function center
	double end = 0;	// Membership function upper bound
	for(int m = 0; m < numMemFunc; m++){

		// Evaluating membership function limits
		beg	= memFuncLim[0];
		mid	= memFuncLim[m];
		end	= memFuncLim[numMemFunc-1];
		if(m != 0)				// Not first membership function
			beg	= memFuncLim[m-1];
		if(m != numMemFunc-1)	// Not last membership function
			end	= memFuncLim[m+1];

		// Evaluating membership values
		if(crispInput <= beg)	// Below lower bound
			memFuncVal[m] = (double)(m == 0);				// 1 if first membership function
		else
		if(crispInput >= end)	// Above upper bound
			memFuncVal[m] = (double)(m == numMemFunc-1);	// 1 if last membership function
		else{
			switch(memFuncType){
				case LIN:{ // Linear membership function
					if(crispInput < mid)
						memFuncVal[m] = (crispInput - beg)/(mid - beg);
					else
						memFuncVal[m] = (crispInput - end)/(mid - end);
					break;
				}
				case SIN:{ // Sinusoid membership function
					if(m == 0)				// First membership function
						beg -= (end - beg);
					if(m == numMemFunc-1)	// Last membership function
						end += (end - beg);
					memFuncVal[m] = pow(sin(PI*(crispInput-beg)/(end - beg)),2);
					break;
				}
				default:{
					memFuncVal[m] = 0;
					break;
				}
			}
		}
	}
}

double FuzzyVar::getValue(int index){
	if(index < 0 || index >=  numMemFunc)
		return 0;
	return memFuncVal[index];
}