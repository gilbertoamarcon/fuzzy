#include "includes.hpp"
#include "fuzzy.hpp"

// Testing range and number of steps
#define MIN1		-45.0
#define MIN2		-30.0
#define MAX1		+45.0
#define MAX2		+30.0
#define NUM_STEPS	+90
#define STEP_SIZE1	(MAX1 - MIN1)/NUM_STEPS
#define STEP_SIZE2	(MAX2 - MIN2)/NUM_STEPS

// Output files
#define TEST_FUZZY_VO	"Files/TEST_FUZZY_VO"
#define TEST_FUZZY_AO	"Files/TEST_FUZZY_AO"

void testFuzzy(char *outputPath,int outputSelect);

int main(int argc, char **argv){

	// Evaluating Velocity output
	testFuzzy(TEST_FUZZY_VO,0);

	// Evaluating Angle output
	testFuzzy(TEST_FUZZY_AO,1);

	return 0;
}

void testFuzzy(char *outputPath,int outputSelect){

	// Fuzzy parameters loading
	Fuzzy fuzzy;
	fuzzy.load(PATH);

	// Opening file
	FILE *file;
	file 	= fopen(outputPath,"w");
	if(file == NULL){
		cout << "Error writing file '" << outputPath << "'." << endl;
		return;
	}
	cout << "Saving file '" << outputPath << "'" << endl;

	// Printing range parameters
	fprintf(file,"MIN1:%*.*f\n",PPS,PP,MIN1);
	fprintf(file,"MAX1:%*.*f\n",PPS,PP,MAX1);
	fprintf(file,"NUM_STEPS:%d\n",NUM_STEPS);
	fprintf(file,"MIN2:%*.*f\n",PPS,PP,MIN2);
	fprintf(file,"MAX2:%*.*f\n",PPS,PP,MAX2);
	fprintf(file,"NUM_STEPS:%d\n",NUM_STEPS);

	// Covering all test range
	for(int i = 0; i <= NUM_STEPS; i++){
		for(int j = 0; j <= NUM_STEPS; j++){

			// Defining inputs
			fuzzy.crispIn[0] = MIN1 + i*STEP_SIZE1;
			fuzzy.crispIn[1] = MIN2 + j*STEP_SIZE2;
			
			// Evaluating output
			fuzzy.eval();

			// Printg output
			fprintf(file,"%*.*f,",PPS,PP,fuzzy.crispOut[outputSelect]);
		}
		fprintf(file,"\n");
	}
	
	// Closing file
	cout << "Map written." << endl;
	fclose(file);
	return;

}