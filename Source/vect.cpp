#include "vect.hpp"

Vect::Vect(){
	this->x = 0;
	this->y = 0;
}

Vect::Vect(double x,double y){
	this->x = x;
	this->y = y;
}

void Vect::init(double x,double y){
	this->x = x;
	this->y = y;
}

void Vect::scale(double s){
	this->x *= s;
	this->y *= s;
}

void Vect::unity(){
	double a = sqrt(pow(x,(double)2)+pow(y,(double)2));
	if(a != 0){
		x /= a;
		y /= a;
	}
}

Vect Vect::copyScaled(double s){
	Vect copy = *this; 
	copy.x *= s;
	copy.y *= s;
	return copy;
}

Vect Vect::copyRotated(Vect dir){
	dir.unity();
	Vect copy = sumVect(dir.copyScaled(x),dir.ortho().copyScaled(-y));
	return copy;
}

Vect Vect::ortho(){
	return Vect(-y,x);
}

double Vect::absolute(){
	return distance(*this,Vect(0,0));
}

double Vect::angle(){
	if(x == 0)
		return 0;
	return atan(y/x);
}

Vect sumVect(Vect a, Vect b){
	return Vect(a.x + b.x,a.y + b.y);
}

Vect subVect(Vect a, Vect b){
	return Vect(a.x - b.x,a.y - b.y);
}

double dotVect(Vect a, Vect b){
	return a.x*b.x + a.y*b.y;
}

double distance(Vect a,Vect b){
	return sqrt(pow(a.x - b.x,(double)2)+pow(a.y - b.y,(double)2));
}

Vect avgVect(Vect a, Vect b){
	Vect c = sumVect(a,b);
	c.scale(0.5);
	return c;
}