#ifndef __VECT_HPP__
#define __VECT_HPP__
#include <stdlib.h>
#include <cstdio>
#include <math.h>

using namespace std;

class Vect{
	public:
		double x;
		double y;
		Vect();
		Vect(double x,double y);
		void init(double x,double y);
		void scale(double s);
		void unity();
		Vect copyScaled(double s);
		Vect copyRotated(Vect dir);
		Vect ortho();
		double absolute();
		double angle();
};

struct Vect3f{
	float x;
	float y;
	float z;
	Vect3f(float x = 0, float y = 0, float z = 0){
		this->x = x;
		this->y = y;
		this->z = z;
	}
	Vect3f minus(const Vect3f &b){
		Vect3f v;
		v.x = this->x - b.x;
		v.y = this->y - b.y;
		v.z = this->z - b.z;
		return v;
	}

	Vect3f plus(const Vect3f &b){
		Vect3f v;
		v.x = this->x + b.x;
		v.y = this->y + b.y;
		v.z = this->z + b.z;
		return v;
	}

	Vect3f divide(const float &b){
		Vect3f v;
		v.x = this->x/b;
		v.y = this->y/b;
		v.z = this->z/b;
		return v;
	}

	Vect3f mul(const float &b){
		Vect3f v;
		v.x = this->x*b;
		v.y = this->y*b;
		v.z = this->z*b;
		return v;
	}

	Vect3f operator-(const Vect3f &b){
		return this->minus(b);
	}

	Vect3f operator+(Vect3f &b){
		return this->plus(b);
	}

	Vect3f operator/(const float &b){
		return this->divide(b);
	}

	Vect3f operator*(float &b){
		return this->mul(b);
	}



	Vect3f cross(Vect3f &b){
		Vect3f v;
		v.x = this->y * b.z - this->z * b.y;
		v.y = this->z * b.x - this->x * b.z;
		v.z = this->x * b.y - this->y * b.x;
		return v;
	}
	float size(){
		return sqrt((this->x * this->x) + (this->y * this->y) + (this->z * this->z));
	}
	Vect3f& normalize(){
		if(this->x != 0.0f || this->y != 0.0f || this->z != 0.0f){//avoids division by 0
			float magnitude = this->size();
			this->x /= magnitude;
			this->y /= magnitude;
			this->z /= magnitude;
		}
		//printf("size = %f\n", this->size());
		return *this;
	}

};

/*Vect3f operator-(Vect3f left, Vect3f right){
	return left.minus(right);
}

Vect3f operator+(Vect3f left, Vect3f right){
	return left.plus(right);
}*/

Vect sumVect(Vect a, Vect b);

Vect subVect(Vect a, Vect b);

double dotVect(Vect a, Vect b);

double distance(Vect a,Vect b);

Vect avgVect(Vect a, Vect b);

#endif
