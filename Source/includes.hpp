#ifndef __INCLUDES_HPP__
#define __INCLUDES_HPP__
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <thread>
#include <random>
#include <algorithm>
#include "GL/freeglut.h"
#include "GL/gl.h"

using namespace std;

#define PI				3.14159265359

// File parameters
#define PATH			"Files/fuzzy_001"

// Printing parameters
#define PP				6
#define PPS				1+2*PP

// File loading parameters
#define BUFFER_SIZE		256

// Video Parameters
#define WINDOW_TITLE	"Video"
#define FULL_SCREEN		1
#define EXIT			27

#endif
