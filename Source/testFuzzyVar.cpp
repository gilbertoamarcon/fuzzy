#include "includes.hpp"
#include "fuzzyVar.hpp"

// Fuzzy parameters
#define NUM_MEM_FUN		11
#define MEM_FUN_WIDTH	7
#define MEM_FUN_OFFSET	35

// Analysis Ranges
#define RANGE_MIN		-60
#define RANGE_MAX		+60
#define NUM_STEPS		1000

// Output files
#define TEST_FUZZY_VAR_LIN	"Files/TEST_FUZZY_VAR_LIN"
#define TEST_FUZZY_VAR_SIN	"Files/TEST_FUZZY_VAR_SIN"

void testFuzzyVar(char *outputPath,int memFuncType);

int main(int argc, char **argv){

	// Testing fuzzy var with linear membership function
	testFuzzyVar(TEST_FUZZY_VAR_LIN,LIN);

	// Testing fuzzy var with sinusoidal membership function
	testFuzzyVar(TEST_FUZZY_VAR_SIN,SIN);

	return 0;

}

void testFuzzyVar(char *outputPath,int memFuncType){

	// Membership function limits
	double memFuncLim[NUM_MEM_FUN];
	for(int m = 0; m < NUM_MEM_FUN; m++)
		memFuncLim[m] = MEM_FUN_WIDTH*m-MEM_FUN_OFFSET;

	// Initializing FuzzyVar object
	FuzzyVar fuzzyVar;
	fuzzyVar.init(memFuncType,NUM_MEM_FUN,memFuncLim);

	// Opening file
	FILE *file 	= fopen(outputPath,"w");
	if(file == NULL){
		cout << "Error writing file '" << outputPath << "'." << endl;
		return;
	}
	cout << "Saving file '" << outputPath << "'" << endl;

	for(int i = 0; i <= NUM_STEPS; i++){

		// Input crisp value
		double crisp = (double)i*(RANGE_MAX-RANGE_MIN)/NUM_STEPS + RANGE_MIN;
		fprintf(file,"%*.*f,",PPS,PP,crisp);

		// Evaluating input and generating output 
		fuzzyVar.eval(crisp);


		// Printing output membership values to file
		for(int m = 0; m < NUM_MEM_FUN; m++){
			fprintf(file,"%*.*f",PPS,PP,fuzzyVar.getValue(m));
			if(m != NUM_MEM_FUN-1) fprintf(file,",");
		}
		fprintf(file,"\n");
	}

	// Closing file
	cout << "File written." << endl;
	fclose(file);

	return;

}