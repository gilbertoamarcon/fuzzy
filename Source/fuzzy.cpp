#include "fuzzy.hpp"

void Fuzzy::init(){
	numRules = 1;
	for(int i = 0; i < numInVar; i++)
		numRules *= fuzzyVar[i].numMemFunc;
	crispIn		= new double[numInVar];
	crispOut	= new double[numOutVar];
	antecedents	= new double[numRules];
	consequents	= new double[numRules*numOutVar];
	mshipIndex	= new int[numRules];
	for(int i = 0; i < numInVar; i++)
		crispIn[i]		= 0;
	for(int i = 0; i < numOutVar; i++)
		crispOut[i]		= 0;
	for(int i = 0; i < numRules; i++){
		antecedents[i]	= 0;
		mshipIndex[i]	= 0;
	}
	for(int i = 0; i < numRules*numOutVar; i++)
		consequents[i]	= 0;
}

void Fuzzy::getRuleVars(int i){
	int shift0 = 1;
	int shift1 = 1;
	for(int v = 0; v < numInVar; v++){
		shift0 = shift1;
		shift1 *= fuzzyVar[v].numMemFunc;
		mshipIndex[v] = i%shift1;
		i -= mshipIndex[v];
		mshipIndex[v] /= shift0;
	}
}

Fuzzy::Fuzzy(){
	this->numRules		= 0;
	this->numInVar		= 0;
	this->numOutVar		= 0;
	this->fuzzyVar		= NULL;
	this->antecedents	= NULL;
	this->consequents	= NULL;
	this->mshipIndex	= NULL;
	this->crispIn		= NULL;
}

Fuzzy::~Fuzzy(){	
	delete[] fuzzyVar;
	delete[] crispIn;
	delete[] crispOut;
	delete[] antecedents;
	delete[] consequents;
	delete[] mshipIndex;
}

int Fuzzy::load(char *path){

	// Aux variables and buffer allocation
	int memFuncType = 0;
	int aux = 0;
	FILE *file;	
	char fileBuffer[BUFFER_SIZE]; 

	// Opening file
	file 	= fopen(path,"r");
	if(file == NULL)
		return 1;

	// File Header
	if(fgets(fileBuffer, BUFFER_SIZE, file) == NULL){
		cout << "Error: Wrong file type." << endl;
		return 1;
	}

	// memFuncType
	if(fgets(fileBuffer, BUFFER_SIZE, file) == NULL){
		cout << "Error: Error while reading file." << endl;
		return 1;
	}
	aux = 0;
	while(fileBuffer[aux++] != ':');
	memFuncType = atoi(fileBuffer+aux);
	if(DEBUG_FUZZY) printf("memFuncType: %d\n",memFuncType);

	// numInVar
	if(fgets(fileBuffer, BUFFER_SIZE, file) == NULL){
		cout << "Error: Error while reading file." << endl;
		return 1;
	}
	aux = 0;
	while(fileBuffer[aux++] != ':');
	numInVar = atoi(fileBuffer+aux);
	if(DEBUG_FUZZY) printf("numInVar: %d\n",numInVar);
	this->fuzzyVar = new FuzzyVar[numInVar];

	// numOutVar
	if(fgets(fileBuffer, BUFFER_SIZE, file) == NULL){
		cout << "Error: Error while reading file." << endl;
		return 1;
	}
	aux = 0;
	while(fileBuffer[aux++] != ':');
	numOutVar = atoi(fileBuffer+aux);
	if(DEBUG_FUZZY) printf("numOutVar: %d\n",numOutVar);

	// Fuzzy variable parameters
	int numMemFunc	= 0;

	// Reading membership limits for each input fuzzy variable 
	for(int v = 0; v < numInVar; v++){

		// Rading file line
		if(fgets(fileBuffer,BUFFER_SIZE,file) == NULL) return(1);

		// Counting how many membership functions
		aux			= 0;
		numMemFunc	= 1;
		while(fileBuffer[aux] != '\n')
			if(fileBuffer[aux++] == ',')
				numMemFunc++;
		if(DEBUG_FUZZY) printf("v(%d): %d numMemFunc: ",v,numMemFunc);

		// Reading membership limits for input fuzzy variable 'v'
		aux			= 0;
		double memFuncLim[numMemFunc];
		for(int s = 0; s < numMemFunc; s++){
			memFuncLim[s] = atof(fileBuffer+aux);
			while(fileBuffer[aux] != ',' && aux++ < BUFFER_SIZE); aux++;
			if(DEBUG_FUZZY) printf("%*.*f,",PPS,PP,memFuncLim[s]);
		}
		if(DEBUG_FUZZY) printf("\n");

		// Fuzzy variable 'v' initialization
		fuzzyVar[v].init(memFuncType,numMemFunc,memFuncLim);
	}
	init();
	if(DEBUG_FUZZY) printf("numRules: %d\n",numRules);

	// Fuzzy rule consequents loading
	for(int i = 0; i < numOutVar*numRules; i++){
		if(fgets(fileBuffer, BUFFER_SIZE, file) == NULL){
			cout << "Error: Error while reading file (consequents)." << endl;
			return 1;
		}
		consequents[i] = atof(fileBuffer);
	}

	// Closing file
	fclose(file);
	return 0;

}

void Fuzzy::eval(){

	// Evaluating each fuzzy output 
	for(int o = 0; o < numOutVar; o++){

		crispOut[o] = 0;
		double accum = 0;

		// Evaluating each rule
		for(int i = 0; i < numRules; i++){

			if(DEBUG_FUZZY) printf("|R%3d|",i);

			antecedents[i]	= 1;
			getRuleVars(i);

			// Evaluating each variable
			for(int v = 0; v < numInVar; v++){

				// Evaluating membership functions for crisp inputs
				fuzzyVar[v].eval(crispIn[v]);

				// Composing antecedents from corresponding membership function output of variable 'v'
				antecedents[i] *= fuzzyVar[v].getValue(mshipIndex[v]);

				if(DEBUG_FUZZY) printf("%2dI%2d|",v,mshipIndex[v]);
				if(DEBUG_FUZZY) printf("M %*.*f|",PPS,PP,fuzzyVar[v].getValue(mshipIndex[v]));
			}

			if(DEBUG_FUZZY) printf("	A: %*.*f",PPS,PP,antecedents[i]);
			if(DEBUG_FUZZY) printf("	C: %*.*f",PPS,PP,consequents[o*numRules+i]);
			if(DEBUG_FUZZY) printf("	P: %*.*f\n",PPS,PP,antecedents[i]*consequents[o*numRules+i]);

			// Composing crisp output
			crispOut[o]	+= antecedents[i]*consequents[o*numRules+i];
			accum		+= antecedents[i];
		}

		// Crisp output normalization (final weighted average step)
		crispOut[o] /= accum;

		if(DEBUG_FUZZY) printf("S %3d: %*.*f\n",o,PPS,PP,crispOut[o]);
	}
}