#ifndef __FUZZY_HPP__
#define __FUZZY_HPP__
#include "includes.hpp"
#include "fuzzyVar.hpp"

#define DEBUG_FUZZY	0

class Fuzzy{

	private:
		int numRules;			// Number of fuzzy rules
		int numInVar;			// Number of input variables
		int numOutVar;			// Number of ouput variables
		FuzzyVar *fuzzyVar;		// Fuzzy input variables
		double *antecedents;	// Antecedent values
		double *consequents;	// Consequents values
		int *mshipIndex;			// 

		// Initialize memmory and compute numRules
		void init();

		// Given a rule index, determine all corresponding membership functions from each fuzzy variable
		void getRuleVars(int i);

	public:
		double *crispIn;		// Crisp inputs
		double *crispOut;		// Crisp ouputs

		// Constructor
		Fuzzy();

		// Destructor
		~Fuzzy();

		// Load fuzzy system parameters from file
		int load(char *path);

		// Evaluate output values from inputs
		void eval();
};

#endif
