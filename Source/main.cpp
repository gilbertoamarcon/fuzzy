#include "includes.hpp"
#include "vect.hpp"
#include "fuzzy.hpp"

int path_numProbes				= 3;
double path_distBetweenProbes	= 0.05;
double field_width				= 2.6;
double field_height				= 1.8;
char* path_hist					= "Files/robotPathHistory";
Fuzzy fuzzy;

int nearestNode = 0;
int *pathProbeIndex;
Vect *pathProbe;

Vect refProbe;
Vect goalVelRobRef;
Vect goalVelGloRef;
Vect robot;
Vect robotT;

vector <Vect> robot_hist;
vector <Vect> robotT_hist;
vector <Vect> goalRobotVel_hist;
vector <double> headDist;

int loadHist(char *path_name);
void nextNode(int *node);
void probePath();
void updateScene();

// Graphics
void iniGl();
void RenderScene();
void keyPressed(unsigned char key, int x, int y);

int main(int argc, char **argv) {

	fuzzy.load(PATH);

	cout << "Loading history path..." << endl;
	loadHist(path_hist);
	cout << "History path loaded." << endl;

	pathProbeIndex	= new int[path_numProbes];
	pathProbe		= new Vect[path_numProbes];
	for(int i = 0; i < path_numProbes; i++)
		pathProbeIndex[i] = 0;

	nearestNode = 0;
	glutInit(&argc, argv);
	iniGl();
	glutMainLoop();

	return 0;
}

int loadHist(char *path_name){

	int aux = 0;
	FILE *file;	
	char fileBuffer[BUFFER_SIZE]; 

	file 	= fopen(path_name,"r");
	if(file == NULL)
		return 1;

	int intAux = 0;
	double auxX = 0;
	double auxY = 0;
	while(fgets(fileBuffer, BUFFER_SIZE, file) != NULL){
		intAux = 0;

		// robot_hist
		auxX = atof(fileBuffer+intAux);
		while(fileBuffer[intAux] != ',' && intAux++ < BUFFER_SIZE); intAux++;
		auxY = atof(fileBuffer+intAux);
		while(fileBuffer[intAux] != ',' && intAux++ < BUFFER_SIZE); intAux++;
		robot_hist.push_back(Vect(auxX,auxY));

	}
	
	fclose(file);
	return 0;
}

void nextNode(int *node){
	(*node)++;
	if((*node) >= robot_hist.size())
		(*node) -= robot_hist.size();
}

void probePath(){
	robot	= robot_hist.at(nearestNode);
	robotT.init(1,0);
	int j = nearestNode;
	int k = nearestNode;
	for(int i = 0; i < path_numProbes; i++){
		j = k;
		nextNode(&k);
		double dist = distance(robot_hist.at(j),robot_hist.at(k));
		while(dist < path_distBetweenProbes){
			j = k;
			nextNode(&k);
			dist += distance(robot_hist.at(j),robot_hist.at(k));
		}
		pathProbeIndex[i] 	= k;
		pathProbe[i]		= subVect(robot_hist.at(k),robot).copyRotated(robotT);
	}
}

void updateScene(){	

	probePath();

	fuzzy.crispIn[0] = pathProbe[1].copyRotated(pathProbe[0]).angle();
	fuzzy.crispIn[1] = pathProbe[2].copyRotated(pathProbe[0]).angle();

	fuzzy.eval();

	double goalVelRobRefAbs		= fuzzy.crispOut[0];
	double goalVelRobRefTheta	= PI*fuzzy.crispOut[1]/180;

	goalVelRobRef.x = goalVelRobRefAbs*cos(goalVelRobRefTheta);
	goalVelRobRef.y = goalVelRobRefAbs*sin(goalVelRobRefTheta);
	goalVelRobRef = goalVelRobRef.copyRotated(pathProbe[0]);
	goalVelGloRef = goalVelRobRef.copyRotated(robotT);
	nextNode(&nearestNode);
}

void iniGl(){
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutInitWindowSize(field_width,field_height);
	glutCreateWindow(WINDOW_TITLE);
	glutDisplayFunc(&RenderScene);
	glutIdleFunc(&RenderScene);
	glutKeyboardFunc(&keyPressed);
	glMatrixMode(GL_PROJECTION);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glDisable(GL_ALPHA_TEST);
	gluOrtho2D(0,field_width,field_height,0);
	if(FULL_SCREEN)
		glutFullScreen();
	glutSetCursor(GLUT_CURSOR_NONE);
}

void RenderScene(){
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	updateScene();
	glPushMatrix();
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluOrtho2D(0,field_width,field_height,0);
		glMatrixMode(GL_MODELVIEW);

			// Path
			glPointSize(1);
			glColor3f(1,1,1);
			glBegin(GL_POINTS);
				for(int i = 0; i < robot_hist.size(); i++)
					glVertex2f(robot_hist.at(i).x,robot_hist.at(i).y);
			glEnd();

			// Robot Global
			glPushMatrix();
				glTranslatef(robot.x,robot.y,0);

				// Global robot
				glPointSize(8);
				glColor3f(1,0,0);
				glBegin(GL_POINTS);
					glVertex2f(0,0);
				glEnd();

				// Global ref vel
				glColor3f(0,1,0);
				glBegin(GL_LINE_LOOP);
					glVertex2f(0,0);
					glVertex2f(goalVelGloRef.x,goalVelGloRef.y);
				glEnd();
			glPopMatrix();

			// Robot Local
			glPushMatrix();
				glTranslatef(field_width/2,field_height/2,0);

				// Local robot
				glPointSize(8);
				glColor3f(1,0,0);
				glBegin(GL_POINTS);
					glVertex2f(0,0);
				glEnd();

				// Local ref vel
				glColor3f(0,1,0);
				glBegin(GL_LINE_LOOP);
					glVertex2f(0,0);
					glVertex2f(goalVelRobRef.x,goalVelRobRef.y);
				glEnd();

				// Local probes	
				glColor3f(0,0,1);	
				for(int i = 0; i < path_numProbes; i++){
					double cIndex = pathProbe[i].absolute();
					double sF = 0.1;
					glPushMatrix();
						glTranslatef(pathProbe[i].x,pathProbe[i].y,0);
						glBegin(GL_LINE_LOOP);
							glVertex3f(-sF*cIndex,-sF*cIndex,0);
							glVertex3f( sF*cIndex,-sF*cIndex,0);
							glVertex3f( sF*cIndex, sF*cIndex,0);
							glVertex3f(-sF*cIndex, sF*cIndex,0);
						glEnd();
					glPopMatrix();
				}

			glPopMatrix();

			// Probe angles
			glPushMatrix();
				glTranslatef(0,field_height/2,0);
				for(int i = 0; i < path_numProbes; i++){
					glColor3f(1,1,1);
					glBegin(GL_LINE_LOOP);
						glVertex3f(0.01*i,0,0);
						glVertex3f(0.01*i,0.5*pathProbe[i].copyRotated(pathProbe[0]).angle(),0);
					glEnd();
				}
			glPopMatrix();

			// Output
			glPushMatrix();
				glTranslatef(0,field_height/2,0);
				for(int i = 0; i < path_numProbes; i++){
					glColor3f(1,1,1);
					glBegin(GL_LINE_LOOP);
						glVertex3f(0,0,0);
						glVertex3f(goalVelRobRef.copyRotated(pathProbe[0]).x,goalVelRobRef.copyRotated(pathProbe[0]).y,0);
					glEnd();
				}
			glPopMatrix();

			// Global probes	
			glColor3f(0,0,1);
			for(int i = 0; i < path_numProbes; i++){
				double cIndex = pathProbe[i].absolute();
				double sF = 0.1;
				glPushMatrix();
					glTranslatef(robot_hist.at(pathProbeIndex[i]).x,robot_hist.at(pathProbeIndex[i]).y,0);
					glBegin(GL_LINE_LOOP);
						glVertex3f(-sF*cIndex,-sF*cIndex,0);
						glVertex3f( sF*cIndex,-sF*cIndex,0);
						glVertex3f( sF*cIndex, sF*cIndex,0);
						glVertex3f(-sF*cIndex, sF*cIndex,0);
					glEnd();
				glPopMatrix();
			}

	glPopMatrix();

	glutSwapBuffers();
}

// When keyboard pressed
void keyPressed(unsigned char key, int x, int y){
	switch (key){
		case EXIT:
			exit(0);
			break;
		default:
			break;
	}
}
