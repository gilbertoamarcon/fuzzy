#ifndef __FUZZYVAR_HPP__
#define __FUZZYVAR_HPP__
#include "includes.hpp"

// Membership function types
#define LIN		0
#define SIN		1

class FuzzyVar{

	private:
		int memFuncType;	// Membership function type
		double *memFuncVal;	// Membership function values
		double *memFuncLim;	// Membership function limits

	public:

		int numMemFunc;	// Number of membership functions

		// Constructor
		FuzzyVar();

		// Destructor
		~FuzzyVar();

		// Define membership function limits and init buffers
		void init(int memFuncType,int numMemFunc,double *memFuncLim);

		// Eval all membership functions for crispInput
		void eval(double crispInput);

		// Get output value for 'index' membership function
		double getValue(int index);
};

#endif
